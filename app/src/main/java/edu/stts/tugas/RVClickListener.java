package edu.stts.tugas;

import android.view.View;

public interface RVClickListener {
    public void recyclerviewClicked(View v, int pos);
}
