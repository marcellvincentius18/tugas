package edu.stts.tugas;

import java.util.ArrayList;

public class Order {
    private String Type;
    private ArrayList<String> Topping = new ArrayList<>();
    private int Qty;
    private int Sub=0;
    private static int id;

    public Order(String type, ArrayList<String> topping, int qty) {
        this.Type = type;
        this.Topping = topping;
        this.Qty = qty;
        this.Sub = hitung(type,topping,qty);
    }

    public int hitung(String type,ArrayList<String> topping,int qty){
        int subtotal =0;
        if(type.equalsIgnoreCase("tea")){
            subtotal+=20000;
        }
        else if(type.equalsIgnoreCase("coffee")){
            subtotal+=25000;
        }
        else{
            subtotal+=30000;
        }
        for (int i= 0; i<topping.size();i++){
            String top= topping.get(i);
            if(top.equalsIgnoreCase("pudding") || top.equalsIgnoreCase("pearl")){
                subtotal+=3000;
            }
            else{
                subtotal+=4000;
            }
        }
        return subtotal*qty;
    }

    public static int getId() {
        return id;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public ArrayList<String> getTopping() {
        return Topping;
    }

    public void setTopping(ArrayList<String> topping) {
        Topping = topping;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        Qty = qty;
    }

    public int getSub() {
        return Sub;
    }

    public void setSub(int sub) {
        Sub = sub;
    }
}
