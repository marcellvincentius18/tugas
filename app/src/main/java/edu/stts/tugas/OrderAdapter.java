package edu.stts.tugas;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder>{
    private ArrayList<Order> orderlist;
    private static RVClickListener mylistener;

    public OrderAdapter(ArrayList<Order> orderlist,RVClickListener exec) {
        this.orderlist=orderlist;
        mylistener=exec;
    }

    @NonNull
    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater =LayoutInflater.from(viewGroup.getContext());
        View v =inflater.inflate(R.layout.rowitem,viewGroup,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderAdapter.ViewHolder viewHolder, int i) {
        viewHolder.tvTypeNQty.setText(orderlist.get(i).getQty()+" - " + orderlist.get(i).getType());
        String topping="";
        for (int j =0; j<orderlist.get(i).getTopping().size();j++){
            if(j<orderlist.get(i).getTopping().size()-1){
                topping+=orderlist.get(i).getTopping().get(j)+" - ";
            }
            else{
                topping+=orderlist.get(i).getTopping().get(j);
            }
        }
        viewHolder.tvToppingList.setText(topping);
        viewHolder.tvSubtotal.setText("Rp. "+ orderlist.get(i).getSub()+",00");
    }

    @Override
    public int getItemCount() {
        return (orderlist!=null)?orderlist.size():0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTypeNQty,tvToppingList,tvSubtotal;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTypeNQty=itemView.findViewById(R.id.tvTypeNQty);
            tvToppingList=itemView.findViewById(R.id.tvToppingList);
            tvSubtotal = itemView.findViewById(R.id.tvSubtotal);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mylistener.recyclerviewClicked(v,ViewHolder.this.getLayoutPosition());
                }
            });
        }
    }
}
