package edu.stts.tugas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
     RecyclerView rView;
     OrderAdapter adapter;
     CheckBox cekPearl,cekPudding,cekReadBean,cekCoconut;
     RadioButton radTea,radCoffee,radSmoothie;
     int index=-1;
     TextView tvQty,tvNama,tvTotal;
     int Total;
     String nama="";
     ArrayList<Order> orderlis = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rView=findViewById(R.id.rView);
        cekPearl=findViewById(R.id.cbPearl);
        cekCoconut=findViewById(R.id.cbCoconut);
        cekPudding=findViewById(R.id.cbPudding);
        cekReadBean=findViewById(R.id.cbRedBean);
        radTea=findViewById(R.id.rbTea);
        radCoffee=findViewById(R.id.rbCoffee);
        radSmoothie=findViewById(R.id.rbSmoothie);
        tvQty=findViewById(R.id.txtQty);
        tvNama=findViewById(R.id.txtNama);
        tvTotal=findViewById(R.id.txtTotal);
        adapter =new OrderAdapter(orderlis, new RVClickListener() {
            @Override
            public void recyclerviewClicked(View v, int pos) {
                index=pos;
                if(orderlis.get(pos).getType().equalsIgnoreCase("tea")){
                    radTea.setChecked(true);
                }
                else if(orderlis.get(pos).getType().equalsIgnoreCase("coffee")){
                    radCoffee.setChecked(true);
                }
                else{
                    radSmoothie.setChecked(true);
                }
                ArrayList<String> topping = new ArrayList<>();
                topping=orderlis.get(pos).getTopping();
                for (int i=0;i<topping.size();i++){
                    if(topping.get(i).toString().equalsIgnoreCase("pudding")){
                        cekPudding.setChecked(true);
                    }
                    if(topping.get(i).toString().equalsIgnoreCase("pearl")){
                        cekPearl.setChecked(true);
                    }
                    if(topping.get(i).toString().equalsIgnoreCase("red bean")){
                        cekReadBean.setChecked(true);
                    }
                    if(topping.get(i).toString().equalsIgnoreCase("coconut jelly")){
                        cekCoconut.setChecked(true);
                    }
                }
                tvQty.setText(orderlis.get(pos).getQty()+"");
            }
        });
        RecyclerView.LayoutManager lm =new LinearLayoutManager(MainActivity.this);
        rView.setLayoutManager(lm);
        rView.setAdapter(adapter);
    }
    public void btnAdd(View v){
        if(tvNama.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(MainActivity.this,"Text box nama harus di isi dahulu",Toast.LENGTH_LONG).show();
        }
        else if(tvNama.getText().toString().equalsIgnoreCase(nama)){
            nama = tvNama.getText().toString();
            String type;
            ArrayList<String> Topping = new ArrayList<>();
            int Qty=Integer.parseInt(tvQty.getText().toString());
            if(radTea.isChecked()){
                type=radTea.getText().toString();
            }
            else if (radCoffee.isChecked()){
                type=radCoffee.getText().toString();
            }
            else{
                type=radSmoothie.getText().toString();
            }
            if(cekReadBean.isChecked()){
                Topping.add(cekReadBean.getText().toString());
            }
            if(cekPudding.isChecked()){
                Topping.add(cekPudding.getText().toString());
            }
            if(cekCoconut.isChecked()){
                Topping.add(cekCoconut.getText().toString());
            }
            if (cekPearl.isChecked()){
                Topping.add(cekPearl.getText().toString());
            }
            orderlis.add(new Order(type,Topping,Qty));
            Toast.makeText(MainActivity.this,orderlis.size()+"",Toast.LENGTH_LONG).show();
            adapter.notifyDataSetChanged();
            Total=hitungTotal(orderlis);
            radTea.setChecked(true);
            cekPearl.setChecked(false);
            cekCoconut.setChecked(false);
            cekPudding.setChecked(false);
            cekReadBean.setChecked(false);
            tvTotal.setText("HI "+nama+", total: Rp. "+Total);
        }
        else{
            orderlis.removeAll(orderlis);
            nama = tvNama.getText().toString();
            String type;
            ArrayList<String> Topping = new ArrayList<>();
            int Qty=Integer.parseInt(tvQty.getText().toString());
            if(radTea.isChecked()){
                type=radTea.getText().toString();
            }
            else if (radCoffee.isChecked()){
                type=radCoffee.getText().toString();
            }
            else{
                type=radSmoothie.getText().toString();
            }
            if(cekReadBean.isChecked()){
                Topping.add(cekReadBean.getText().toString());
            }
            if(cekPudding.isChecked()){
                Topping.add(cekPudding.getText().toString());
            }
            if(cekCoconut.isChecked()){
                Topping.add(cekCoconut.getText().toString());
            }
            if (cekPearl.isChecked()){
                Topping.add(cekPearl.getText().toString());
            }
            orderlis.add(new Order(type,Topping,Qty));
            Toast.makeText(MainActivity.this,orderlis.size()+"",Toast.LENGTH_LONG).show();
            adapter.notifyDataSetChanged();
            Total=hitungTotal(orderlis);
            radTea.setChecked(true);
            cekPearl.setChecked(false);
            cekCoconut.setChecked(false);
            cekPudding.setChecked(false);
            cekReadBean.setChecked(false);
            tvTotal.setText("HI "+nama+", total: Rp. "+Total);
        }
    }
    public int hitungTotal(ArrayList<Order>orderlist){
        int total=0;
        for (int i=0;i<orderlist.size();i++){
            total+=orderlist.get(i).getSub();
        }
        return total;
    }
    public void btnMin(View v){
        int qty=Integer.parseInt(tvQty.getText().toString());
        if(qty>1){
            qty--;
        }
        tvQty.setText(qty+"");
    }
    public void btnPlus(View v){
        int qty=Integer.parseInt(tvQty.getText().toString());
        qty++;
        tvQty.setText(qty+"");
    }

    public void btnReset(View v){
        orderlis.removeAll(orderlis);
        radTea.setChecked(true);
        cekPearl.setChecked(false);
        cekCoconut.setChecked(false);
        cekPudding.setChecked(false);
        cekReadBean.setChecked(false);
        tvQty.setText("1");
        tvNama.setText("");
        tvTotal.setText("");
        adapter.notifyDataSetChanged();
    }

    public void btnDel(View v){
        if(index!=-1){
            orderlis.remove(index);
            Total = hitungTotal(orderlis);
            radTea.setChecked(true);
            cekPearl.setChecked(false);
            cekCoconut.setChecked(false);
            cekPudding.setChecked(false);
            cekReadBean.setChecked(false);
            tvQty.setText("1");
            tvTotal.setText("HI "+nama+", total: Rp. "+Total);
            adapter.notifyDataSetChanged();
            index=-1;
        }
        else{
            Toast.makeText(MainActivity.this,"Pilih order yang mau di delete dahulu",Toast.LENGTH_LONG).show();
        }
    }

}
